# rasp-mode

An emacs major mode for [RASP](https://github.com/tech-srl/RASP) (Restricted Access Sequence Processing Language), a language for understanding *transformers*.


# Features

- Syntax highlighting (currently supported : keywords, builtins, variable and function declarations, constants, comments, strings)
- Commenting with <kbd>M-x comment-dwim</kbd>
- Auto-indentation
- Auto-completion of rasp builtins (keywords, builtins and constants), in-buffer variables and functions.


# Installation

You can install rasp-mode using <kbd>M-x package-install-file</kbd>. Put `(require 'rasp)` in your `.emacs` file to load the package when emacs starts. You can use rasp-mode with <kbd>M-x rasp-mode</kbd>, but it should automatically activate when opening files ending in `.rasp`.
